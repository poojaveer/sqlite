﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SQLIte
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string path = @"D:\mySQLiteDatabase.sqlite";

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path + ";Version=3;"); //Create the connection with database
                string myQuery = "create table student (Id int, Name varchar(50), Address varchar(50))"; //SQL query to create a table with three columns . delete this after first execution
                dbConnection.Open();
                //if (!File.Exists(path))
                //{
                SQLiteCommand command = new SQLiteCommand(myQuery, dbConnection);//Create a SQLite command which accepts the query and database connection
                command.ExecuteNonQuery();

                //}
            }
            catch (Exception ex)
            {

            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path + ";Version=3;"); //Create the connection with database
                dbConnection.Open();
                //Open the connection with database
                string myQuery1 = "insert into student (Id, Name, Address) values(001,'" + txtName.Text + "', 'Pune')"; //SQL query to insert data
                SQLiteCommand command1 = new SQLiteCommand(myQuery1, dbConnection);//Create a SQLite command which accepts the query and database connection.
                command1.ExecuteNonQuery();//Executes the SQL query
                dbConnection.Close();


            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        public static bool TableExists(String tableName, SQLiteConnection connection)
        {
            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.CommandText = "SELECT * FROM sqlite_master WHERE type = 'table' AND name = @name";
                cmd.Parameters.AddWithValue("@name", tableName);

                using (SQLiteDataReader sqlDataReader = cmd.ExecuteReader())
                {
                    if (sqlDataReader.Read())
                        return true;
                    else
                        return false;
                }
            }
        }

        private void btnRetrive_Click(object sender, RoutedEventArgs e)
        {

            SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path + ";Version=3;"); //Create the connection with database
            string myQuery = "select * from student"; //SQL query to retrieve data from table
            SQLiteCommand command = new SQLiteCommand(myQuery, dbConnection);//Create a SQLite command which accepts the query and database connection.
            dbConnection.Open();//Open the connection with database
            SQLiteDataReader dataReader = command.ExecuteReader();//Executes the SQL query

            dbConnection.Close();//Close the connection with database
        }
    }
}
